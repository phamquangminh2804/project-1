﻿create database QL_KinhThoiTrang
Use QL_KinhThoiTrang

CREATE TABLE KHACHHANG
(
	MAKHACHHANG nvarchar(50) NOT NULL,
	TENKHACHHANG nvarchar(50) NOT NULL,
	GIOITINH nvarchar(50) NOT NULL,
	DIACHI nvarchar(50) NOT NULL,
	EMAIL nvarchar(50),
	DIENTHOAI char(10),   
	Constraint pk_khachang_makhachhang PRIMARY KEY (MAKHACHHANG)
)
GO

CREATE TABLE NHANVIEN
(
	MANHANVIEN nvarchar(50) NOT NULL,
	TENNHANVIEN nvarchar(30) NOT NULL,
	GIOITINH nvarchar(50) NOT NULL,
	NGAYSINH DATE ,
	NGAYLAMVIEC DATE,
	DIACHI nvarchar(50) NOT NULL,
	DIENTHOAI char(10),
	LUONGCOBAN varchar(50),
	PHUCAP varchar(50),
	Constraint pk_nhanvien_manhanvien PRIMARY KEY (MANHANVIEN)
)
GO

CREATE TABLE NHASANXUAT
(
	MANHASANXUAT nvarchar(50) NOT NULL,
	TENNHASANXUAT nvarchar(50) NOT NULL,
	DIACHI nvarchar(50) NOT NULL,
	DIENTHOAI char(10),  
	EMAIL nvarchar(50),
	Constraint pk_nhasanxuat_manhasanxuat PRIMARY KEY (MANHASANXUAT)
)
GO

CREATE TABLE CHATLIEU
(
	MACHATLIEU nvarchar(50) NOT NULL,
	TENCHATLIEU nvarchar(50) NOT NULL,
	Constraint pk_chatlieu_machatlieu PRIMARY KEY (MACHATLIEU)
)
GO

CREATE TABLE SANPHAM
(
	MASANPHAM nvarchar(50) NOT NULL,
	TENSANPHAM nvarchar(50) NOT NULL,
	MANHASANXUAT nvarchar(50) NOT NULL,
	MACHATLIEU nvarchar(50) NOT NULL,
	MAUSAC nvarchar(50) NOT NULL,
	HINHDANG nvarchar(50) NOT NULL,
	SOLUONG int NOT NULL,
	DONVITINH varchar(50) NOT NULL,
	GIAHANG varchar(50) NOT NULL,
	Constraint pk_sanpham_masanpham PRIMARY KEY (MASANPHAM),
	Constraint fk_sanpham_manhasanxuat FOREIGN KEY (MANHASANXUAT) REFERENCES NHASANXUAT(MANHASANXUAT),
	Constraint fk_sanpham_machatlieu FOREIGN KEY (MACHATLIEU) REFERENCES CHATLIEU(MACHATLIEU)
)
GO
CREATE TABLE HOADONBAN
(
	SOHOADON int,
	MAKHACHHANG nvarchar(50) NOT NULL,
	MANHANVIEN nvarchar(50) NOT NULL,
	NGAYDATHANG DATE,
	NGAYGIAOHANG DATE,
	NGAYCHUYENHANG DATE,
	NOIGIAOHANG nvarchar(50) NOT NULL,
	Constraint pk_hoadonban_sohoadon PRIMARY KEY (SOHOADON),
	Constraint fk_hoadonban_makhachhang FOREIGN KEY (MAKHACHHANG) REFERENCES KHACHHANG(MAKHACHHANG),
	Constraint fk_hoadonban_manhanvien FOREIGN KEY (MANHANVIEN) REFERENCES NHANVIEN(MANHANVIEN)
)
GO



CREATE TABLE CHITIETHOADONBAN
(
	SOHOADON int,
	MASANPHAM nvarchar(50) NOT NULL,
	GIABAN varchar(50) NOT NULL,
	SOLUONG int NOT NULL,
	MUCGIAMGIA varchar(50) NOT NULL,
	Constraint pk_chitiethoadonban PRIMARY KEY (SOHOADON,MASANPHAM),
	Constraint fk_chitiethoadonban_sohoadon FOREIGN KEY (SOHOADON) REFERENCES HOADONBAN(SOHOADON),
	Constraint fk_chitiethoadonban_masanpham FOREIGN KEY (MASANPHAM) REFERENCES SANPHAM(MASANPHAM)
)
GO

----INSERT---------
INSERT INTO KHACHHANG(MAKHACHHANG,TENKHACHHANG,GIOITINH,DIACHI,EMAIL,DIENTHOAI)
VALUES ('KH001',N'Phạm Thị Loan',N'Nữ',N'Hà Nội','loan@gmail.com','0904842648'),
		('KH002',N'Đoàn Văn Trường',N'Nam',N'Đống Đa','truong@gmail.com','0905557573'),
		('KH003',N'Lê Văn Luyện',N'Nam',N'Thanh Xuân','luyen@gmail.com','0909999323'),
		('KH004',N'Võ Thị Sáu',N'Nữ',N'Hồ Chí Minh','sau@gmail.com','0904846455'),
		('KH005',N'Nguyễn Đức Nam',N'Nam',N'Vĩnh Phúc','nam@gmail.com','0904844532')


INSERT INTO NHANVIEN(MANHANVIEN,TENNHANVIEN,GIOITINH,NGAYSINH,NGAYLAMVIEC,DIACHI,DIENTHOAI,LUONGCOBAN,PHUCAP)
VALUES('NV001',N'Nguyễn Đức Anh',N'Nam','3-15-1989','6-20-2007',N'Hà Nội','0324567891',5000000,100000),
	  ('NV002',N'Nguyễn Đức Phúc',N'Nam','6-16-1973','8-20-2001',N'Sơn La','0224567826',10000000,500000),
	  ('NV003',N'Trần Văn Chiến',N'Nam','5-15-2000','8-22-2022',N'Hà Nội','0324567749',4000000,200000),
	  ('NV004',N'Phạm Văn Nữ',N'Nữ','4-20-1966','2-19-1999',N'Ứng Hòa','0324566132',15000000,400000),
	  ('NV005',N'Nguyễn Thanh Huyền',N'Nữ','3-15-1966','7-25-1987',N'Hồ Chí Minh','0324569821',8000000,500000)
		

INSERT INTO NHASANXUAT(MANHASANXUAT,TENNHASANXUAT,DIACHI,DIENTHOAI,EMAIL)
VALUES ('GC',N'Gucci',N'Hà Nội','0332916789','gucci@gmail.com'),
	   ('DO',N'Dior',N'Hồ Chí Minh','0342916788','dior@gmail.com'),
	   ('LV',N'Louis Vuitton',N'Hà Nội','0352916612','louisvuitton@gmail.com'),
	   ('BB',N'Burberry',N'Vĩnh Phúc','0332919632','burberry@gmail.com'),
	   ('DG',N'Dolce & Gabbana',N'Sơn La','0332919845','dolceGabbana@gmail.com'),
	   ('LA',N'Lacoste',N'Ứng Hóa','0332913131','lacoste@gmail.com'),
	   ('GM',N'Gentle Monster',N'Hồ Chí Minh','0325254188','gentleMonster@gmail.com')

INSERT INTO CHATLIEU(MACHATLIEU,TENCHATLIEU)
VALUES ('PU',N'Nhựa'),
		('AL',N'Nhôm'),
		('TI',N'Titan'),
		('TG',N'Thép không gỉ')


INSERT INTO SANPHAM(MASANPHAM,TENSANPHAM,MANHASANXUAT,MACHATLIEU,MAUSAC,HINHDANG,SOLUONG,DONVITINH,GIAHANG)
VALUES ('GG04',N'Kính mát Gucci GG0402SK','GC','PU','màu hồng','Tròn',100,'VND',13000000),
		('GG06',N'Kính mát Gucci GG0697S','GC','TG','màu đen','Vuông',20,'VND',10000000),
		('DG43',N'Kính mát Dolce & Gabbana DG4335F','DG','TG','màu đen','Mắt mèo',30,'VND',8000000),
		('DG22',N'Kính mát Dolce & Gabbana DG2214','DG','PU','màu vàng','Vuông',20,'VND',11000000),
		('DOB2U',N'Kính mát Dior MISSDIOR B2U','DO','TI','màu nâu','Mắt mèo',40,'VND',12000000),
		('DOKB7',N'Kính mát Dior DIORLIA KB7','DO','TG','màu bạc','Phi công',20,'VND',6000000),
		('GMKC1',N'Kính mát Gentle Monster Billy KC1','GM','TG','màu xanh lá','Vuông',30,'VND',8200000),
		('GMY2',N'Kính mát Gentle Monster Mill Y2','GM','PU','màu đen','Tròn',20,'VND',5500000)

INSERT INTO HOADONBAN(SOHOADON,MAKHACHHANG,MANHANVIEN,NGAYDATHANG,NGAYGIAOHANG,NGAYCHUYENHANG,NOIGIAOHANG)
VALUES (8,'KH001','NV001','6-22-2020','6-25-2020','6-23-2020',N'Hà Nội'),
		(10,'KH001','NV002','5-10-2010','5-15-2010','5-12-2010',N'Hà Nội'),
		(15,'KH003','NV004','1-13-2020','1-16-2020','1-14-2020',N'Hồ Chí Minh'),
		(3,'KH005','NV002','7-15-2008','7-19-2008','7-17-2008',N'Đà Nẵng'),
		(11,'KH002','NV003','8-25-2000','8-30-2000','8-26-2000',N'Đống Đa'),
		(5,'KH002','NV004','11-25-2000','11-30-2000','11-28-2000',N'Cao Bằng'),
		(6,'KH004','NV004','12-14-2011','12-18-2011','12-16-2000',N'Đồng Nai')


INSERT INTO CHITIETHOADONBAN(SOHOADON,MASANPHAM,GIABAN,SOLUONG,MUCGIAMGIA)
VALUES (8,'GG04',13000000,10,0.5),
		(10,'GG06',10000000,20,0),
		(15,'GG04',13000000,5,0.1),
		(3,'DG43',8000000,3,0.1),
		(11,'DG22',11000000,11,0.4),
		(5,'DG43',8000000,5,0.2),
		(6,'GMKC1',8200000,7,0.5)


--cau1: cho danh sach nhan vien'nam' cua cua hang va nam lam viec>10 nam
select*from nhanvien
select Tennhanvien,gioitinh,YEAR(GETDATE())-YEAR(NGAYLAMVIEC) as[nam lam viec]
from nhanvien
group by tennhanvien,gioitinh,NGAYLAMVIEC
having gioitinh='nam' and YEAR(GETDATE())-YEAR(NGAYLAMVIEC)>10

							
--cau2: dua ra mat hang ban nhieu nhat va tong tien da ban
select masanpham,sum(soluong) as [tong],sum(soluong*giaban-mucgiamgia*soluong*giaban) as [tongtien]
from CHITIETHOADONBAN
group by masanpham
having sum(soluong)>=all(select sum(soluong)
						from CHITIETHOADONBAN
						group by masanpham)

--câu 3 : Danh sách các mặt hàng chưa được đặt bao giờ
SELECT MASANPHAM, TENSANPHAM
FROM SANPHAM
WHERE MASANPHAM NOT IN (SELECT DISTINCT MASANPHAM FROM CHITIETHOADONBAN)
-- câu 4 : cho biết mã và tên các mặt hàng có giá trị lớn hơn 1000000 và có số lượng ít hơn 50

SELECT MASANPHAM, TENSANPHAM
FROM SANPHAM
WHERE GIAHANG>1000000 AND SOLUONG>20

--Cau5: Tính tổng số tiền đã mua hàng của từng khách hàng, thống kê dựa vào tên khách hàng
SELECT TENKHACHHANG , 
SUM(GIABAN*SOLUONG) 
AS 'TỔNG TIỀN'FROM (KHACHHANG KH INNER JOIN HOADONBAN HDB ON KH.MAKHACHHANG=HDB.MAKHACHHANG)
INNER JOIN CHITIETHOADONBAN CT ON HDB.SOHOADON=CT.SOHOADONGROUP BY TENKHACHHANG
--Cau 6Tính tổng số lượng và tổng số tiền đã bán được của từng mặt hàng trong năm 2020

SELECT MASANPHAM,
SUM(SOLUONG) AS 'TỔNG SỐ LƯỢNG',
SUM(SOLUONG*GIABAN-SOLUONG*MUCGIAMGIA) AS 'TỔNG SỐ TIỀN'
from (CHITIETHOADONBAN CT INNER JOIN HOADONBAN HDB ON CT.SOHOADON=HDB.SOHOADON)
WHERE  YEAR(NGAYDATHANG) = '2020'
GROUP BY MASANPHAM

--Cau 7: Cho biết tổng số tiền hàng mà cửa hàng thu đc trong mỗi tháng của năm 2000 (thời gian đc tính theo ngày đặt hàng)
SELECT MONTH(NGAYDATHANG) AS THANG,
SUM(SOLUONG*GIABAN-MUCGIAMGIA*SOLUONG*GIABAN) as 'Tổng tiền'
FROM (HOADONBAN HDB INNER JOIN CHITIETHOADONBAN CTHDB
ON HDB.SOHOADON= CTHDB.SOHOADON)
WHERE YEAR(NGAYDATHANG)=2000
GROUP BY MONTH(NGAYDATHANG)

--Cau 8: SAN PHAM TEN 'Kính mát Gucci GG0402SK' DO AI DAT VA DO NHAN VIEN NAO THIET LAP, DIA DIEM GIAO HANG O DAU--
SELECT TENKHACHHANG, TENNHANVIEN,NOIGIAOHANG
FROM (((KHACHHANG KH INNER JOIN HOADONBAN HDB
ON  KH.MAKHACHHANG=HDB.MAKHACHHANG) INNER JOIN NHANVIEN NV
ON NV.MANHANVIEN = HDB.MANHANVIEN) INNER JOIN CHITIETHOADONBAN CTHDB
ON HDB.SOHOADON = CTHDB.SOHOADON) INNER JOIN SANPHAM SP
ON CTHDB.MASANPHAM = SP.MASANPHAM
WHERE TENSANPHAM= 'Kính mát Gucci GG0402SK'

select*from sanpham






--thu tuc 

-- tinh tong so luong 2 san pham
alter proc soluong2sanpham(@masanpham1 nvarchar(50),@masanpham2
nvarchar(50))
as
declare @soluong1 int
declare @soluong2 int
declare @tong int
select @soluong1=SOLUONG from SANPHAM where MASANPHAM=@masanpham1
select @soluong2=SOLUONG from SANPHAM where MASANPHAM=@masanpham2
select @tong=@soluong1+@soluong2
print N'tổng số lượng là: ' + str(@tong)
soluong2sanpham GG04,GG06

 

 -- kiem tra xem so tuoi 2 nhân viên có bằng nhau không
alter proc kiemtratuoi(@MANHANVIEN1 Nvarchar(50), @MANHANVIEN2 NVARCHAR(50))
as 
declare @TUOINHANVIEN1 INT
DECLARE @TUOINHANVIEN2 INT

SELECT @TUOINHANVIEN1=(YEAR(GETDATE())-YEAR(NGAYSINH)) FROM NHANVIEN WHERE MANHANVIEN=@MANHANVIEN1
SELECT @TUOINHANVIEN2=(YEAR(GETDATE())-YEAR(NGAYSINH)) FROM NHANVIEN WHERE MANHANVIEN=@MANHANVIEN2
PRINT 'TUOI NHAN VIEN '+@MANHANVIEN1+'LA'+ STR(@TUOINHANVIEN1)
PRINT 'TUOI NHAN VIEN '+@MANHANVIEN2+'LA'+ STR(@TUOINHANVIEN2)

IF @TUOINHANVIEN1=@TUOINHANVIEN2 PRINT '2 NHAN VIEN '+@MANHANVIEN1+'VA' +@MANHANVIEN2+'CUNG TUOI'

IF @TUOINHANVIEN1>@TUOINHANVIEN2 PRINT ' NHAN VIEN '+@MANHANVIEN1+'LON HON TUOI' +@MANHANVIEN2

IF @TUOINHANVIEN1<@TUOINHANVIEN2 PRINT 'NHAN VIEN '+@MANHANVIEN1+'NHO HON TUOI' +@MANHANVIEN2

select* from nhanvien

kiemtratuoi NV001,NV005


select*from khachhang
--Tính tổng tiền đã mua hàng của một khách hàng nào đó theo mã KH
alter PROCEDURE TONGTIENDMCUAKH (@MAKH VARCHAR(50))
AS	
	DECLARE @TONG INT
	SELECT @TONG=sum(SOLUONG*GIABAN-MUCGIAMGIA*SOLUONG*GIABAN) 
	FROM CHITIETHOADONBAN CTHDB ,HOADONBAN HDB ,KHACHHANG KH 
	WHERE CTHDB.SOHOADON=HDB.SOHOADON AND
		HDB.MAKHACHHANG=KH.MAKHACHHANG AND
		@MAKH=kh.MAKHACHHANG
	PRINT N'TONG TIEN ĐÃ MUA :'+STR(@TONG)
	tongtiendmcuakh KH001
	

select*from nhanvien
--tinh so nam lam viec cua mot nhan vien nao do theo manhanvien
alter procedure sonamlamviec(@MANHANVIEN nvarchar(50))
as
	declare @Namlamviec int
	select @Namlamviec=(YEAR(GETDATE())-YEAR(NGAYLAMVIEC)) from nhanvien where manhanvien=@MANHANVIEN
PRINT 'nam lam viec cua nhan vien'+ @MANHANVIEN+ ' la:' + str(@Namlamviec)
sonamlamviec NV001

--so sanh 2 san pham xem san pham nao ban chay hon
select*from CHITIETHOADONBAN
alter proc kiemtrasanpham(@MASANPHAM1 Nvarchar(50), @MASANPHAM2 NVARCHAR(50))
as 
declare @SOLUONG1 INT
DECLARE @SOLUONG2 INT

SELECT @SOLUONG1=sum(soluong) FROM CHITIETHOADONBAN WHERE MASANPHAM=@MASANPHAM1
SELECT @SOLUONG2=sum(soluong) FROM CHITIETHOADONBAN WHERE MASANPHAM=@MASANPHAM2
PRINT 'So luong san pham '+@MASANPHAM1+ ' LA'+ STR(@SOLUONG1)
PRINT 'So luong san pham '+@MASANPHAM2+ ' LA'+ STR(@SOLUONG2)

IF @SOLUONG1=@SOLUONG2 PRINT '2 San pham '+@MASANPHAM1+'VA' +@MASANPHAM2+'ban chay ngang ngau'

else IF @SOLUONG1>@SOLUONG2 PRINT 'san pham'+@MASANPHAM1+' ban chay hon san pham' +@MASANPHAM2

else IF @SOLUONG1<@SOLUONG2 PRINT 'San pham '+@MASANPHAM2+' ban chay hon san pham ' +@MASANPHAM1


kiemtrasanpham GMKC1,GG06
--tao VIEW vw_tblNhaSX1 gồm (TENSANPHAM,SOLUONG)dùng để thống kê số lượng sản phẩm theo tên sản phẩm và tên nhà sản xuất
create view vw_tblNhaSX1
as
 select  TENNHASANXUAT, TENSANPHAM, SOLUONG
 FROM NHASANXUAT NSX INNER JOIN SANPHAM SP ON NSX.MANHASANXUAT=SP.MANHASANXUAT
 where  SOLUONG<100

  SELECT * FROM vw_tblNhaSX1

 --tao VIEW vw_tblKhachHang gồm (TENKHACHHANG,TONGTIEN) thong ke so tien tung khach hang da mua

create view vw_tblKhachHang
as
	SELECT TENKHACHHANG,SUM(SOLUONG*GIABAN-MUCGIAMGIA*SOLUONG*GIABAN) as 'Tổng tiền'
	FROM (KHACHHANG KH INNER JOIN HOADONBAN HDB ON KH.MAKHACHHANG=HDB.MAKHACHHANG)
	INNER JOIN CHITIETHOADONBAN CT ON HDB.SOHOADON=CT.SOHOADON
	group by TENKHACHHANG
select * from vw_tblKhachHang

--SỐ LƯỢNG TỒN KHO THEO TUNG SẢN PHẨM
alter VIEW vw_tkHangTonKho
as
SELECT SP.MASANPHAM,SP.TENSANPHAM
FROM SANPHAM SP, CHITIETHOADONBAN CT
WHERE SP.MASANPHAM=CT.MASANPHAM
select*from vw_tkHangTonKho

--so luong ton kho của từng ma sản phẩm
create view vw_tonkho
as
SELECT A.MASANPHAM, A.SOLUONG AS TONDAU,
	(SELECT SUM(SOLUONG) FROM SANPHAM WHERE MASANPHAM = A.MASANPHAM )AS NHAP,
	(SELECT SUM(SOLUONG) FROM CHITIETHOADONBAN WHERE MASANPHAM = A.MASANPHAM )AS XUAT,
	(
		(SELECT SUM(SOLUONG) FROM SANPHAM WHERE MASANPHAM = A.MASANPHAM )
		-(SELECT SUM(SOLUONG) FROM CHITIETHOADONBAN WHERE MASANPHAM = A.MASANPHAM )
		) AS TONCUOI
		FROM SANPHAM A
SELECT * FROM vw_tonkho

select*from hoadonban
select*from CHITIETHOADONBAN
select*from sanpham
select*from khachhang

create trigger update_giahang
on sanpham
for update
as
	if update(giahang)
	update CHITIETHOADONBAN
	set giaban= inserted.giahang
	from(inserted inner join chitiethoadonban on chitiethoadonban.masanpham=inserted.masanpham)

update sanpham
set giahang='12000'
where masanpham='DG43'

   

-- View vw_DMSP gồm (MASANPHAM,TENSANPHAM) dùng để liệt kê các danh sách  sản phẩm hiện có trong bảng SANPHAM

create view vw_DMSP1
as
	select TENSANPHAM,MASANPHAM
	from sanpham


Select *From vw_DMSP1

select*from hoadonban
select*from CHITIETHOADONBAN
select*from khachhang



--	Tạo View vw_TONGTIENKH gồm (TENKHACHHANG,TONG TIEN) dung để Tính tổng số tiền đã mua hàng của từng khách hàng theo tên khách hàng

select*from khachhang

create view vw_TONGTIENKH1
as 
	select kh.TENKHACHHANG,sum(GIABAN*SOLUONG-MUCGIAMGIA*GIABAN*SOLUONG) as [TONG TIEN]
	from CHITIETHOADONBAN cthd,KHACHHANG kh,hoadonban hdb
	where cthd.sohoadon=hdb.sohoadon and kh.makhachhang=hdb.makhachhang
	group by kh.tenkhachhang

select*from vw_TONGTIENKH1



select*from nhanvien
--	Tính số năm làm việc của một nhân viên bất kì theo mã nhân viên

create procedure sonamlamvie(@MANHANVIEN nvarchar(50))
AS 
declare @namlamviec int
	select @namlamviec=(YEAR(GETDATE())-YEAR(NGAYLAMVIEC))
	from nhanvien
	where MANHANVIEN=@MANHANVIEN
print 'namlamviec cua nhan vien' +@MANHANVIEN+ 'la' +str(@namlamviec)
sonamlamvie NV001

select*from hoadonban
select*from CHITIETHOADONBAN
select*from khachhang

--	Kiểm tra xem 2 sản phẩm xem sản phẩm nào bán chạy hơn

create procedure kiemtra(@MASANPHAM1 nvarchar(50),@MASANPHAM2 nvarchar(50))
as
	declare @SOLUONG1 int;
	declare @SOLUONG2 int;
	
	select @SOLUONG1= sum(soluong) from CHITIETHOADONBAN where MASANPHAM=@MASANPHAM1
	select @SOLUONG2= sum(soluong) from CHITIETHOADONBAN where MASANPHAM=@MASANPHAM2


select*from nhanvien
--Tính số năm làm việc của một nhân viên bất kì theo mã nhân viên
create procedure sonam(@manhanvien nvarchar(50))
as
	declare @sonam int;
	select (YEAR(GETDATE())-YEAR(NGAYLAMVIEC)) from nhanvien where manhanvien=@manhanvien
	print 'SO NAM LAM VIEN CUA' +@manhanvien +'la' +str(@sonam)

sonam NV001



	select manhanvien,luongcoban,phucap from nhanvien
order by luongcoban asc